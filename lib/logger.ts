import type { Logging } from "./interface/logging";
import type { Observability } from "./interface/observability";
import type { Printer } from "./interface/printer";

import { LogLevel } from "./log-level";

export class Logger implements Logging {
  constructor(
    private logLevel: LogLevel,
    private printer?: Printer,
    private observability?: Observability
  ) {}

  fatal(...data: unknown[]) {
    if (!this.allowed(LogLevel.FATAL)) {
      return;
    }
    this.printer?.error(data);
    this.observability?.logMessage(LogLevel.FATAL, "Fatal exception occurred");
    this.observability?.captureException(data);
  }

  error(err: unknown | unknown[]) {
    if (!this.allowed(LogLevel.ERROR)) {
      return;
    }
    this.printer?.error(err);
    this.observability?.captureException(err);
  }

  warn(...data: unknown[]) {
    if (!this.allowed(LogLevel.WARNING)) {
      return;
    }
    this.printer?.warn(...data);
    this.observability?.logMessage(LogLevel.WARNING, ...data);
  }

  info(...data: unknown[]) {
    if (!this.allowed(LogLevel.INFO)) {
      return;
    }
    this.printer?.info(...data);
    this.observability?.logMessage(LogLevel.INFO, ...data);
  }

  debug(...data: unknown[]) {
    if (!this.allowed(LogLevel.DEBUG)) {
      return;
    }
    this.printer?.debug(...data);
    this.observability?.logMessage(LogLevel.DEBUG, ...data);
  }

  private allowed(logLevel: LogLevel) {
    return LogPriority[logLevel] >= LogPriority[this.logLevel];
  }
}

// maps LogLevel to priority for determining whether a log should be printed
enum LogPriority {
  FATAL = 50,
  ERROR = 40,
  WARNING = 30,
  INFO = 20,
  DEBUG = 10,
}
