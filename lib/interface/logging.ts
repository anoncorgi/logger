/**
 * The main interface for application logging.
 *
 * Under the hood, interfaces with Observability and Printer.
 *
 * Will not emit logs below set log level.
 */
export interface Logging {
  // Severe errors that cause premature termination.
  fatal(...data: unknown[]): void;

  // Runtime errors or unexpected conditions.
  error(err: unknown): void;

  // Use of deprecated APIs, poor use of API, 'almost' errors, other runtime situations that are undesirable or unexpected, but not necessarily "wrong".
  warn(...data: unknown[]): void;

  // Interesting runtime events (startup/shutdown). Should be used sparingly.
  info(...data: unknown[]): void;

  // Detailed information on the flow through the system.
  debug(...data: unknown[]): void;
}