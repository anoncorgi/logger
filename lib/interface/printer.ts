/**
 * The interface for printing logs.
 *
 * Most common example is the browser console API.
 *
 * Can be silenced with the squelch argument.
 */
export interface Printer {
  // Runtime errors or unexpected conditions
  error(err: unknown): void;

  // Use of deprecated APIs, poor use of API, 'almost' errors, other runtime situations that are undesirable or unexpected, but not necessarily "wrong".
  warn(...data: unknown[]): void;

  // Interesting runtime events (startup/shutdown). Should be used sparingly.
  info(...data: unknown[]): void;

  // Detailed information on the flow through the system.
  debug(...data: unknown[]): void;
}