import type { AppConfig } from "@/core/config";
import type { Observability, Printer } from "@/lib/logger";

import { config } from "@/core/config";
import { Logger } from "@/lib/logger";

import { SentryWrapper } from "./sentry-wrapper";

export const logger = createLogger(config);

// ###############
// ### Private ###
// ###############

// `createLogger` instantiates the `Logger` singleton for the application.
function createLogger(config: AppConfig): Logger {
  const printer = getPrinter(config);
  const observability = getObservability(config);

  const logger = new Logger(config.logLevel, printer, observability);

  logger.info("[Logger started!]");

  return logger;
}

// `getPrinter` provides a Print service in the Printer interface
// If the "squelch" flag is set, no service is provided.
function getPrinter(config: AppConfig): Printer | undefined {
  // Never print messages to the console if "squelch" env is set
  if (config.squelchConsole) {
    return;
  }
  return console;
}

// `getObservability` is the interface between 3rd party monitoring and Logger
function getObservability(config: AppConfig): Observability | undefined {
  try {
    const obs = new SentryWrapper({
      dsn: config.sentryDsn,
      release: config.appVersion,
      environment: config.appEnvironment,
    });
    obs.init();
    return obs;
  } catch (err) {
    // This should never happen because Sentry has its own try/catch internally,
    // but be defensive anyway in case something changes.
    console.warn("Sentry initialization failed.", err.message);
  }
}

