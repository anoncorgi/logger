import type { LogLevel } from "../log-level";

export type Context = {
  [key: string]: unknown;
};

/**
 * Interface for 3rd party observability APIs.
 * e.g. Datadog, Sentry
 */
export interface Observability {
  // initializes 3rd party service
  init(): void;

  // sets global user context
  setUserContext(user: { id?: string, username?: string, email?: string, role?: string }): void;

  // emit error event to observability platform
  captureException(err: unknown, context?: Context): void;

  // emit message to observability platform, with severity level
  logMessage(level: LogLevel, ...data: unknown[]): void;
}