/**
 * `LogLevel` indicates the urgency of a log message.
 *
 * Applications may set LOG_LEVEL in the ENV file to control which logs are, and any messages below this level will not be logged.
 *
 * For example, `.env.production` config may have `LOG_LEVEL=ERROR`, while `env.development` may have LOG_LEVEL=DEBUG.
 */
export enum LogLevel {
  FATAL = "FATAL",
  ERROR = "ERROR",
  WARNING = "WARNING",
  INFO = "INFO",
  DEBUG = "DEBUG"
}