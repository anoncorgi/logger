import type { Context, Observability } from "../../lib/interface/observability";
import { LogLevel } from "../../lib/log-level";

import { BrowserOptions, captureException, captureMessage, configureScope, init, SeverityLevel } from "@sentry/browser";

export class SentryWrapper implements Observability {
  constructor(private opts: BrowserOptions) {}

  init() {
    init(this.opts);
  }

  setUserContext(user: { id?: string, username?: string, email?: string, role?: string }) {
    configureScope(scope => {
      scope.setUser(user);
    });
  }

  captureException(err: unknown, context?: Context) {
    captureException(err, context);
  }

  logMessage(logLevel: LogLevel, ...data: unknown[]) {
    captureMessage(data.join(), this.getSeverityLevel(logLevel));
  }

  // maps Logger's log level to Sentry's severity level
  private getSeverityLevel(logLevel: LogLevel): SeverityLevel {
    switch (logLevel) {
      case LogLevel.FATAL:
        return "fatal";
      case LogLevel.ERROR:
        return "error";
      case LogLevel.WARNING:
        return "warning";
      case LogLevel.INFO:
        return "info";
      case LogLevel.DEBUG:
        return "debug";
    }
  }
}
